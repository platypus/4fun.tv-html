var $window = $(window),
    $body = $('body'),
    F = {
      v: "v0.1.8",
      DEBUG: false,
      _: function(){
        if (F.DEBUG && window.console){
          console.log.apply(console, arguments);
        }
      },
      map: null,
      settings: {},
      defaults: {
        defaultLocation: [51.7595728,19.4787901], // Łódź
        popupPath: "__popup.html",
        markersPath: "assets/js/markers.json",
        markerIcons: {
          tv: {
            size: [66, 45],
            anchor: [33, 45],
            url: "assets/images/markers/tv.png",
            hits: {
              size: [126, 45],
              anchor: [63, 45],
              url: "assets/images/markers/tv-hits.png"
            },
            fit: {
              size: [146, 45],
              anchor: [73, 45],
              url: "assets/images/markers/tv-fit.png"
            }
          },
          fit: {
            size: [86, 45],
            anchor: [43, 45],
            url: "assets/images/markers/fit.png",
            hits: {
              size: [146, 45],
              anchor: [73, 45],
              url: "assets/images/markers/fit-hits.png"
            }
          },
          hits: {
            size: [76, 45],
            anchor: [38, 45],
            url: "assets/images/markers/hits.png"
          },
          all: {
            size: [186, 45],
            anchor: [93, 45],
            url: "assets/images/markers/all.png"
          }
        }
      },
      els: {
        carousel: $('#carousel'),
        mapWrapper: $('#map')
      },
      modules: {
        carousel: function(){
          var slickSettings = {
                autoplay: true,
                autoplaySpeed: 5000,
                prevArrow: '<button type="button" class="slick-prev"><i><span class="sr-only">Poprzedni</span></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i><span class="sr-only">Następny</span></i></button>'
              };

          // debug info
          F._("Load carousel", slickSettings);

          // init
          F.els.carousel.slick(slickSettings);
        },
        instagram: function(){
          var $insta = $('.tile--instagram .tile-carousel');
          if ($insta.length){
            $insta.slick({
              autoplay: true,
              autoplaySpeed: $insta.data('timeout') || 1500,
              arrows: false
            });
          }
        },
        maps: {
          load: function(){
            F._("Load GoogleMaps", (typeof google === 'object' && typeof google.maps === 'object'));

            if (typeof google === 'object' && typeof google.maps === 'object') {
              F.modules.maps.init();
            } else {
              $.ajax({
                url: "//maps.google.com/maps/api/js?sensor=true&callback=F.modules.maps.init",
                timeout: 2000,
                dataType: 'script',
                crossDomain: true
              }).fail(function(a, b, c){
                F.els.mapWrapper.hide();
              });
            }
          },
          icon: function(arr){
            var chosen,
                length = arr.length;

            if (length === 1){
              chosen = F.settings.markerIcons[arr[0]];
            } else if (length === 2){
              chosen = F.settings.markerIcons[arr[0]][arr[1]];
            } else {
              chosen = F.settings.markerIcons.all;
            }

            return {
              origin: new google.maps.Point(0, 0),
              size: new google.maps.Size(chosen.size[0], chosen.size[1]),
              anchor: new google.maps.Point(chosen.anchor[0], chosen.anchor[1]),
              url: chosen.url
            };
          },
          init: function(){
            F._("GoogleMaps API Loaded, init.");
            F._("Center on default position:", F.settings.defaultLocation);

            var mapOptions = {
                  mapTypeControl: false,
                  scaleControl: false,
                  zoom: 12,
                  zoomControl: true,
                  zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                  }
                };

            // center on Gdańsk
            mapOptions.center =  new google.maps.LatLng(F.settings.defaultLocation[0], F.settings.defaultLocation[1]);
            F.map = new google.maps.Map(F.els.mapWrapper[0], mapOptions);

            // duplicate just to simplify things
            F.settings.markerIcons.fit.tv = F.settings.markerIcons.tv.fit;
            F.settings.markerIcons.hits.tv = F.settings.markerIcons.tv.hits;
            F.settings.markerIcons.hits.fit = F.settings.markerIcons.fit.hits;

            // load json with markers
            $.ajax({
              url: F.settings.markersPath,
              type: 'GET',
              timeout: 2000,
              dataType: 'json',
              crossDomain: true
            }).done(function(data){

              if (data.hasOwnProperty("markers")){
                var length = data.markers.length,
                    bounds = new google.maps.LatLngBounds();

                // generate markers
                for (var i = 0, len = length; i < len; i++){
                  var m = data.markers[i],
                      position = new google.maps.LatLng(m.location[0], m.location[1]);

                  // first marker
                  var marker = new google.maps.Marker({
                        id: m.id,
                        position: position,
                        title: m.name,
                        icon: F.modules.maps.icon(m.stations),
                        zIndex: i+1
                      });

                  // extend bounds
                  bounds.extend(position);

                  // attach to Map
                  marker.setMap(F.map);

                  // onClick actions
                  google.maps.event.addListener(marker, 'click', (function(marker, i){
                    return function(){
                      F._("Marker click triggered", marker.id);

                      // remove previous one
                      $('#mapPopup').remove();

                      // load new one
                      $.ajax({
                        url: F.settings.popupPath,
                        data: {
                          id: marker.id
                        },
                        type: 'GET',
                        timeout: 2000
                      }).done(function(data){
                        // append popup
                        $body.addClass('hidden-overflow').append(data);
                        var $popup = $('#mapPopup');

                        // fade in
                        $popup.stop(true, true).fadeIn(500);

                        // close actions
                        $popup.find('.close').off('click.close').on('click.close', function(e){
                          e.preventDefault();

                          $body.removeClass('hidden-overflow');
                          $popup.stop(true, true).fadeOut(500, function(){
                            $popup.remove();
                          });
                        });
                      }).fail(function(a, b, c){
                        F._("Nie udało się załadować popupu:", c);
                      })
                    };
                  })(marker, i));
                }

                // fit map do bounds
                F.map.fitBounds(bounds);
              }
            }).fail(function(a, b, c){
              F._("Nie udało się załadować markerów.", c);
            });
          }
        }
      },
      onReady: function(){
        F._("4fun.tv scripts. Ready.", F.v);

        // home page carousel
        if (F.els.carousel.length){
          F.modules.carousel();
        }

        // intagram init
        F.modules.instagram();

        // google maps page
        if (F.els.mapWrapper.length){
          F.settings = $.extend(true, {}, F.defaults, MapsOptions);
          F.modules.maps.load();
        }

        // home page footer
        $('.page-home .footer').affix({
				  offset: {
				    top: $window.height()
				  }
				});
      },
      onResize: function(){}
    };

// INIT
on_resize(F.onResize)();
$(document).ready(F.onReady);
