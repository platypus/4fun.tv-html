// Include gulp
console.time('gulp');
var gulp = require('gulp'); 
console.timeEnd('gulp');

// Include Our Plugins
console.time('Loading plugins');
console.time('Loading1');

var jshint         = require('gulp-jshint');

console.timeEnd('Loading1');
console.time('Loading2');

var less           = require('gulp-less');

console.timeEnd('Loading2');
console.time('Loading3');

var autoprefixer   = require('gulp-autoprefixer');

console.timeEnd('Loading3');
console.time('Loading4');

var minify         = require('gulp-minify-css');

console.timeEnd('Loading4');
console.time('Loading5');

var concat         = require('gulp-concat');

console.timeEnd('Loading5');
console.time('Loading6');

var uglify         = require('gulp-uglify');

console.timeEnd('Loading6');
console.time('Loading7');

var rename         = require('gulp-rename');

console.timeEnd('Loading7');
console.time('Loading8');

var sourcemaps     = require('gulp-sourcemaps');

console.timeEnd('Loading8');
console.time('Loading11');

var del            = require('del');

console.timeEnd('Loading11');
console.time('Loading12');

var notify         = require('gulp-notify');

console.timeEnd('Loading12');
console.timeEnd('Loading plugins');

// Paths
var paths = {
      build: {
        js: 'build/js',
        css: 'build/less',
        images: 'build/images'
      },
      output: {
        js: 'assets/js',
        css: 'assets/css',
        images: 'assets/images'
      }
    };

function showError(error){
  console.info(error.fileName+':'+error.lineNumber);
  console.error(error.message);
  this.emit('end');
}

// Lint Task
gulp.task('lint', function() {
  return gulp.src(paths.build.js + '/10_functions.js')
          .pipe(jshint())
          .pipe(jshint.reporter('default'));
});

// Compile Our Less
gulp.task('less', function() {
  return gulp.src([
          paths.build.css + '/style.less',
          paths.build.css + '/ie.less'
        ])
        .pipe(less({compress: true}).on('error', showError))//.on('error', gutil.log))
        .pipe(autoprefixer({
          browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'],
          cascade: false
        }).on('error', showError))
        .pipe(minify().on('error', showError))
        .pipe(gulp.dest(paths.output.css))
        .pipe(notify('Less compiled, prefixed and minified'));
});

// Images
gulp.task('images', function () {
    var imagemin       = require('gulp-imagemin'),
        pngquant       = require('imagemin-pngquant');

    return gulp.src(paths.build.images + '/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }).on('error', showError))
        .pipe(gulp.dest(paths.output.images));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
  console.time('scripts');
  
  var pipe = gulp.src(paths.build.js + '/*.js')
            .pipe(concat('app.js'))
            .pipe(gulp.dest(paths.output.js))
            .pipe(uglify().on('error', showError))
            .pipe(rename('app.min.js'))
            .pipe(gulp.dest(paths.output.js))
            .pipe(notify('Scripts compiled and minified -> app.js'));

  console.timeEnd('scripts');
  return pipe;
});

// Bootstrap
gulp.task('bootstrap', function() {

  var arr = [
        paths.build.js + '/lib/bootstrap/affix.js',
        paths.build.js + '/lib/bootstrap/collapse.js',
        paths.build.js + '/lib/bootstrap/transition.js',
        paths.build.js + '/lib/bootstrap/scrollspy.js'
      ];

  return gulp.src(arr)
      .pipe(concat('06_bootstrap.js'))
      .pipe(gulp.dest(paths.build.js))
      .pipe(notify('Scripts compiled and minified -> 06_bootstrap.js'));
});

gulp.task('iefixes', function() {
  return gulp.src(paths.build.js + '/ie/iefixes.js')
          .pipe(concat('iefixes.js'))
          .pipe(gulp.dest(paths.output.js+'/ie'))
          .pipe(uglify().on('error', showError)) //.on('error', gutil.log))
          .pipe(rename('iefixes.min.js'))
          .pipe(gulp.dest(paths.output.js+'/ie'))
          .pipe(notify('Scripts compiled and minified -> iefixes.js'));
});

gulp.task('clean:images', function (cb) {
  del([
    paths.output.images
  ], cb);
});

// Watch Files For Changes
gulp.task('watch', function(){
  gulp.watch(paths.build.css+ '/**/*.less', ['less']);
  gulp.watch(paths.build.js + '/**/*.js',   ['scripts']);
});

// gulp.task('watch_js', function(){
//   gulp.watch(paths.build.js + '/**/*.js',   ['scripts']);
// });

// Default Task
gulp.task('default', ['less', 'scripts', 'watch']);
gulp.task('imgs', ['clean:images', 'images']);