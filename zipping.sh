#! /bin/bash
TODAY=$(date +'%Y%m%d-%H%M%S')
cd "`dirname "$0"`"
mkdir '_zipped'
zip -vr _zipped/"${PWD##*/}"-$TODAY.zip * -x '*.svn*' -x '*@psd*' -x '*_psd*' -x '*_zipped*' -x '*.DS_Store*' -x '*.sass-cache*' -x '*config.codekit*' -x '*prepros.cfg*' -x '*config.rb*' -x '*node_modules*' -x '*zipping.sh*' -x 'build/*' -x '*.sublime-*'